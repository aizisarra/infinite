
function forTouchstart(evt) {
    var target = $(evt.currentTarget);
    target.trigger("click");
}

$('body').on('touchstart', '.menu-item .toolbar-icon', forTouchstart);

$( document ).ready(function() {

    if (jQuery(window).width() < 500) {
        $('.field--name-field-maps-description').hide();
        $('.maps-pictos').hide();



        $('.field--name-field-maps-title-residence').click(function() {
            this.classList.toggle("active");
            if($('.field--name-field-maps-description:visible').length != 0) {
                $('.field--name-field-maps-description').slideUp("normal");

            } else {
                $('.field--name-field-maps-description').slideDown("normal");
            }
            if ($('.maps-pictos:visible')){
                $('.maps-pictos').slideUp("normal");
                $('.field--name-field-maps-title-acces').removeClass("active");
            }

        });

        $('.field--name-field-maps-title-acces').click(function() {
            this.classList.toggle("active");
            if($('.maps-pictos:visible').length != 0) {
                $('.maps-pictos:visible').slideUp("normal");
            }else {
                $('.maps-pictos').slideDown("normal");

            }
            if ($('.field--name-field-maps-description:visible')){
                $('.field--name-field-maps-description').slideUp("normal");
                $('.field--name-field-maps-title-residence').removeClass("active");
            }
        });

        for (var i=0; i < $('.services-content').children().children().children().length; i++) {
            if (i<4) {
                $('.views-row:nth-child('+i+')').show();
            } else if(i>= 4) {
                $('.views-row:nth-child('+i+')').hide();

            }
        }


        $('.services_link').click(function() {
            this.classList.toggle("active");
            if($('.views-row:visible').length > 5)
            {
                for (var i=0; i < $('.services-content').children().children().children().length; i++) {
                    if (i<4) {
                        $('.views-row:nth-child('+i+')').slideDown();
                    } else {
                        $('.views-row:nth-child('+i+')').slideUp("normal");

                    }
                }
            }
            else
            {
                for (var i=0; i < $('.services-content').children().children().children().length; i++) {
                    if (i>=4) {
                        $('.views-row:nth-child('+i+')').slideDown("normal");
                    }
                }
            }
        });
    }

});



