$( document ).ready(function() {

    $(window).scroll(function () {

        //Chiffres animés home

        //Si l'élément est visible on déclenche la fonction
        if(($('.animated_number_top').visible2(true, false) && !$('.animated_number_top').hasClass('activeAnimated'))) {
            $('.animated_number_top').addClass('activeAnimated');

            $('.counter_residence').counterUp({
                delay: 10,
                time:  1000
            });

            $('.counter_collab').counterUp({
                delay:   10,
                time:    1000,
                beginAt: 3000
            });

            $('.counter_customers').counterUp({
                delay: 10,
                time:  1000
            });
        }


    });
});

